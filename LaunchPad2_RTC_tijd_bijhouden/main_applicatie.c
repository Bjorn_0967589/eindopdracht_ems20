// C header files
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
// Driver Header files
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
#include <ti/drivers/net/wifi/slnetifwifi.h>
// RTOS header files
#include <ti/sysbios/BIOS.h>
// POSIX Header files
#include <ti/posix/ccs/pthread.h>
#include <ti/posix/ccs/semaphore.h>
#include <mqueue.h>

// Eigen headers
#include "uart_term.h"
#include "bestanden.h"
#include "Board.h"
#include <ti/drivers/gpio/GPIOCC32XX.h>

// I2C header
#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CCC32XX.h>
#include <unistd.h> // header voor de sleep functie

// Taken
void *App_Task(void *args);
void *I2C(void *args);

// Functies
int StelWifiIn(SlWlanSecParams_t *SecParams, SlWlanSecParamsExt_t *SecExtParams, _i8 *wachtwoord, bool vraagOmWachtwoord);
void MaakTaak(_u8 prioriteit, _u16 stackSize, void *(*functie)(void *args));

//      ******GLOBAL variabelen*******
mqd_t postbus;
mqd_t pakket;

//      ********structs********
typedef struct {
    int jaar, maand, dag, uur, minuut, sec, jaar_rest;
}datum_tijd;



// Interrupt functie van de knop.
void GPIO_15_buttonfxn0(uint_least8_t index)
{
    char save[1];
    save[0] = 's';
    mq_send(postbus, (char *)&save, sizeof(save), NULL);

}

// Zet decimale om in BCD.
void BCD(datum_tijd *a)
{
    a->jaar_rest =(a->jaar/100);
    a->jaar =(a->jaar % 100);
    a->jaar =((a->jaar/10)<<4 | a->jaar%10);
    a->maand =((a->maand/10)<<4 | a->maand%10);
    a->dag =((a->dag/10)<<4 | a->dag%10);
    a->uur =(~(1<<6) & ((a->uur/10)<<4 | a->uur%10));
    a->minuut =((a->minuut/10)<<4 | a->minuut%10);
    a->sec =((1<<7) | ((a->sec/10)<<4 | a->sec%10));
}

// Filtert de bits die niet nodig zijn eruit.(LPYR en ST)
void DECI(datum_tijd *b)
{
    b->sec = (~(1<<7) & b->sec);
    b->maand = (~(1<<5) & b->maand);
}

// Semaphore voor taaksynchronisatie
sem_t wachtenOpIP;

// Wifi instellingen
#define IDENTITY "Bernie"
#define SSID "OnePlus 7 Pro"


void *mainThread(void *arg0)
{
// Configureren van GPIO_15.
    // Zet GPIO_15 als een input met pulldown weerstand en met een interrupt op de falling edge zie CC3220S_LAUNCHXL.c.
    GPIO_init();
    // Maakt een interrupt aan.
    GPIO_setCallback(GPIO_15_IN, GPIO_15_buttonfxn0);
    GPIO_clearInt(GPIO_15_IN);
    GPIO_enableInt(GPIO_15_IN);

    //settings voor de mailbox
    mq_attr settings = {0, 5, 25};

    // initialisatie mqueue
    postbus = mq_open("tijd en datum", O_RDWR | O_CREAT, 0666, &settings);
    pakket = mq_open("ophalen", O_RDWR | O_CREAT, 0666, &settings);

    // Variabelen
    int Status;
    SlWlanSecParams_t SecParams; // Wifi parameters
    SlWlanSecParamsExt_t SecExtParams; // Wifi enterprise parameters
    _i8 ww[64]; // Buffer om een wifi-wachtwoord in op te slaan

    // Intialisaties
    sem_init(&wachtenOpIP, 0, 0); // Semaphore voor synchronisatie
    SPI_init();
    InitTerm(); // UART functionaliteit van TI

    // Prio stack functie
    MaakTaak(9, 2048, sl_Task); // Nodig voor wifi api.
    MaakTaak(2, 2048, App_Task); // Een taak voor het communiceren via socket-communicatie.
    MaakTaak(2, 2048, I2C); // Een taak voor het aansturen van de RTC.


    printf("\nApplicatie gestart.\nOverige meldingen zijn te vinden op de UART terminal op 115200 baud.\n");
    UART_PRINT("Starten.\r\n");

    bool vraagOmWachtwoord = false; // Probeer eerst om opgeslagen wachtwoord te gebruiken
    do
    {
        // Wifi instellen en verbinden
        StelWifiIn(&SecParams, &SecExtParams, ww, vraagOmWachtwoord);

        Status = sl_WlanConnect((_i8*)SSID, // SSID
                                strlen(SSID), // Lengte van SSID
                                0,
                                &SecParams, // Security parameters wifi
                                0); // Extended security parameters enterprice wifi
        if (Status !=0)
        {
            UART_PRINT("Wifi instellingen fout: %d\r\n", Status);
            vraagOmWachtwoord = true; // Probeer nogmaals om een correct wachtwoord in te voeren
        }
    }
    while (Status != 0);

    /*!
     * De thread kan nu stoppen.
     * Wachten op succesvolle verbinding zodat de App-thread
     * verder kan gaan.
     *
     * @see: SimpleLinkNetAppEventHandler(),
     * @see: App_Task(void * args)
     *
     */
    return 0;
}


void *App_Task(void *args)
{
    _u16 len = sizeof(SlNetCfgIpV4Args_t);
    _u16 ConfigOpt = 0;
    char buf[] = "Bjorn van Buren"; // Naam die gestuurd wordt om te checken of er verbinding is gemaakt
    char rec[25];

    SlSockAddrIn_t mijnAdres;   // voor een IPv4 socket
    SlNetCfgIpV4Args_t ipV4 = {0}; // Struct voor ip

    /* Wachten op een ip adres, daarna kunnen we verder.
     * Zie ook SimpleLinkNetAppEventHandler() */
    sem_wait(&wachtenOpIP);

    // Wachtwoord alleen opslaan als verbinden gelukt is.
    schrijfWachtwoord();

    // Haal IP op
    sl_NetCfgGet(SL_NETCFG_IPV4_STA_ADDR_MODE,
                 &ConfigOpt,
                 &len,
                 (_u8 *)&ipV4
                );

    // Pretty print IP
    UART_PRINT( "\r\n"
                "Mijn IP\t %d.%d.%d.%d\r\n"
                "MASK\t %d.%d.%d.%d\r\n"
                "GW\t %d.%d.%d.%d\r\n"
                "DNS\t %d.%d.%d.%d\r\n",
                SL_IPV4_BYTE(ipV4.Ip,3), SL_IPV4_BYTE(ipV4.Ip,2), SL_IPV4_BYTE(ipV4.Ip,1), SL_IPV4_BYTE(ipV4.Ip,0),
                SL_IPV4_BYTE(ipV4.IpMask,3), SL_IPV4_BYTE(ipV4.IpMask,2), SL_IPV4_BYTE(ipV4.IpMask,1), SL_IPV4_BYTE(ipV4.IpMask,0),
                SL_IPV4_BYTE(ipV4.IpGateway,3), SL_IPV4_BYTE(ipV4.IpGateway,2), SL_IPV4_BYTE(ipV4.IpGateway,1), SL_IPV4_BYTE(ipV4.IpGateway,0),
                SL_IPV4_BYTE(ipV4.IpDnsServer,3), SL_IPV4_BYTE(ipV4.IpDnsServer,2), SL_IPV4_BYTE(ipV4.IpDnsServer,1), SL_IPV4_BYTE(ipV4.IpDnsServer,0));


    // Socket uitvoeren
    _i16 socket = sl_Socket(SL_AF_INET, SL_SOCK_STREAM, SL_IPPROTO_TCP);
    if(socket < 0)
    {
        while(1);
    }

    // Server instellingen.
    mijnAdres.sin_family = SL_AF_INET;
    mijnAdres.sin_port = sl_Htons(2000);
    mijnAdres.sin_addr.s_addr = sl_Htonl(SL_INADDR_ANY);


    // Koppelt het socket-adres(mijnAdres) met de al eerder aangemaakte socket.
   _i16 bind = sl_Bind(socket, (SlSockAddr_t *)&mijnAdres, sizeof(SlSockAddrIn_t));
   if(bind < 0)
   {
       while(1);
   }
   // Zorgt ervoor dat er actief wordt geluisterd op poort 2000 waardoor clienten een verbinding kunnen aanvragen en hierop een antwoord krijgen.
   _i16 listen = sl_Listen(socket, 1);
   if(listen < 0)
    {
        while(1);
    }

   // Accepteerd een aanvraag.
   _i16 Thijs = sl_Accept(socket, NULL, 0);
   if(Thijs < 0)
   {
       while(1);
   }
   // Stuurt een bericht naar de client.
   _i16 status = sl_Send(Thijs, buf, sizeof(buf), 0);
   if(status < 0)
   {
       while(1);
   }

   while(1)
   {
       // Ontvangt de berichten van de client.
       _i16 recv = sl_Recv(Thijs, rec, sizeof(rec), 0);
       if(recv < 0)
       {
           while(1);
       }

       /* Als de ID letter van het bericht 'i' is, dan zal de App_Task dit bericht versturen naar de I2C task.
        * Als de ID letter van het bericht 'g' is dan zal de App_Task dit bericht versturen via de mq 'postbus' naar de I2C taak.
        * I2C zal dan via de mq 'pakket' de opgeslagen tijd en datum naar de App_Task sturen.
        * De tijd en datum wordt dan verstuurd naar de client.
        */
       if(rec[0] == 'i')
       {
           mq_send(postbus, (char *)&rec, sizeof(rec), NULL);
       }
       else if(rec[0] == 'g')
       {
         // Stuurt de 'g' door naar de taak 'I2C' .
         int foutmelding = mq_send(postbus, (char *)&rec, sizeof(rec), NULL);
         if (foutmelding < 0)
         {
             while(1);
         }
         // Ontvangt de opgeslagen tijd en datum van de taak 'I2C' .
             foutmelding = mq_receive(pakket, (char *)&rec, sizeof(rec), NULL);
           if (foutmelding < 0)
           {
               while(1);
           }
         // Stuurt de opgeslagen tijd en datum naar de client.
          _i16 status = sl_Send(Thijs, rec, sizeof(rec), 0);
          if(status < 0)
          {
              while(1);
          }
          UART_PRINT("\r\n"
                  "Ik heb dit bericht gestuurd: %s\n", rec);
       }

  }
}

void *I2C(void *args)
{
    int init = 0;
    char output[25];
    char saved_time[25];
    datum_tijd splitter;
    /* Initialiseerd de struct met 0. Zodat de tijd 0000-00-00 00:00:00 wordt.
     * Wanneer er een 'g' wordt ontvangen voordat eerst een 'i' ontvangen is, zal dit aangeven dat er nog geen tijd opgeslagen is.
     */
    datum_tijd merger = {0};

    // I2C init
    bool status;
    I2C_init();
    I2C_Handle i2cHandle;

    //default setting worden hier gebruikt.
    i2cHandle = I2C_open(0, NULL);

    if (i2cHandle == NULL)
    {
        // Error opening I2C
        while(1) {}
    }
    I2C_Transaction i2cTransaction = {0};
    i2cTransaction.slaveAddress = 0b1101111;
    uint8_t readBuffer[8];
    uint8_t writeBuffer[8];

    while(1)
    {
        // Ontvangt de berichten die van de App-Task komen.
       int  foutmelding = mq_receive(postbus, (char *)&output, sizeof(output), NULL);
       if (foutmelding < 0)
       {
           while(1);
       }

        if (output[0] == 'i')
        {
            UART_PRINT("\r\n"
                    "Ik heb dit bericht ontvangen: %s\n", output);

            /* Als de character in rec[0] een 'i' dan zal met behulp van sscanf de datum en tijd worden omgezet in decimalen.
             * De decimalen worden daarna omgezet in hexadecimalen en in de postbus verstuurd naar I2C threadt. */
           int deci = sscanf(output, "i%d-%d-%d %d:%d:%d", &splitter.jaar, &splitter.maand, &splitter.dag, &splitter.uur, &splitter.minuut, &splitter.sec);
           if(deci < 6)
           {
               while(1);
           }
           BCD(&splitter);
    /* Kijken of de oscillator nog disabled moet worden.
     * Dit zodat er voorkomt kan worden dat er een rollover issue onstaat, als er nieuw waarden in de RTC wordt gezet.
     */
            // checken of de ST = 0
            writeBuffer[0] = 0x00;
            i2cTransaction.writeBuf = writeBuffer;
            i2cTransaction.writeCount = 1;
            i2cTransaction.readBuf = readBuffer;
            i2cTransaction.readCount = 1;
            status = I2C_transfer(i2cHandle, &i2cTransaction);
            if (status == false)
            {
                UART_PRINT("\r\n fout: I2C");
               while(1); // Unsuccesful I2C transfer
            }
            // Is ST = 1 dan wordt de ST op 0 gezet.
            if((readBuffer[0] & 1<<7) != 0<<7)
            {
                writeBuffer[0] = 0x00;  // Adres van register
                writeBuffer[1] = 0x00;  // Waarde 0x00 invoeren in het register
                i2cTransaction.writeBuf = writeBuffer;
                i2cTransaction.writeCount = 2;
                i2cTransaction.readBuf = readBuffer;
                i2cTransaction.readCount = 4;
                status = I2C_transfer(i2cHandle, &i2cTransaction);
                if (status == false)
                {
                    UART_PRINT("\r\n fout: I2C");
                   while(1); // Unsuccesful I2C transfer
                }
            }
            // Kijken of de oscillator gestopt is.
            writeBuffer[0] = 0x00;
            i2cTransaction.writeBuf = writeBuffer;
            i2cTransaction.writeCount = 1;
            i2cTransaction.readBuf = readBuffer;
            i2cTransaction.readCount = 4;
            status = I2C_transfer(i2cHandle, &i2cTransaction);
            if (status == false)
            {
                UART_PRINT("\r\n fout: I2C");
               while(1);// Unsuccesful I2C transfer
            }
            // Indien nodig nog wachten totdat de oscillator gestopt is.
            while((readBuffer[3] & 1<<5) != 0<<5)
            {
                usleep(100);
                writeBuffer[0] = 0x00;
                i2cTransaction.writeBuf = writeBuffer;
                i2cTransaction.writeCount = 1;
                i2cTransaction.readBuf = readBuffer;
                i2cTransaction.readCount = 2;
                status = I2C_transfer(i2cHandle, &i2cTransaction);
                if (status == false)
                {
                    UART_PRINT("\r\n fout: I2C");
                   while(1);// Unsuccesful I2C transfer
                }
            }

            // RTC initialiseren.
            writeBuffer[0] = 0x00;
            writeBuffer[1] = splitter.sec;
            writeBuffer[2] = splitter.minuut;
            writeBuffer[3] = splitter.uur;
            writeBuffer[4] = 1; //Omdat er niks wordt gedaan met de weekday, maar wel een waarde (1-7) nodig heeft, is hier voor de waarde 1 gekozen.
            writeBuffer[5] = splitter.dag;
            writeBuffer[6] = splitter.maand;
            writeBuffer[7] = splitter.jaar;

            i2cTransaction.writeBuf = writeBuffer;
            i2cTransaction.writeCount = 8;
            i2cTransaction.readCount = 0;
            status = I2C_transfer(i2cHandle, &i2cTransaction);
            if (status == false)
            {
                UART_PRINT("\r\n fout: I2C");
               while(1);// Unsuccesful I2C transfer
            }
            UART_PRINT("\r\n Init RTC klaar\n\r");
            init = 1;
        }

        /* Zet de opgeslagen tijd en datum(in BCD) om in een string wanneer eer 'g' gestuurd wordt. */
        else if (output[0] == 'g')
        {
            UART_PRINT("\r\n"
                    "Ik heb dit bericht ontvangen: %s\n", output);

            int str = sprintf(saved_time, "%02d%02x-%02x-%02x %02x:%02x:%02x",merger.jaar_rest, merger.jaar,
                              merger.maand, merger.dag, merger.uur, merger.minuut, merger.sec);
            if(str < 0)
            {
                while(1);
            }

            // opgeslagen tijd en datum versturen.
            int foutmelding = mq_send(pakket, (char *)&saved_time, sizeof(saved_time), NULL);
            if (foutmelding < 0)
            {
                while(1);
            }
        }

        else if (output[0] == 's' & init > 0)
        {
            // Tijd en datum uit de RTC lezen.
            writeBuffer[0] = 0x00;
            i2cTransaction.writeBuf = writeBuffer;
            i2cTransaction.writeCount = 1;
            i2cTransaction.readBuf = readBuffer;
            i2cTransaction.readCount = 8;

            status = I2C_transfer(i2cHandle, &i2cTransaction);
            if (status == false)
            {
                UART_PRINT("\r\n fout: I2C");
               while(1);// Unsuccesful I2C transfer
            }
            // Slaat de tijd en datum op in een struct.
            merger.sec = readBuffer[0];
            merger.minuut = readBuffer[1];
            merger.uur = readBuffer[2];
            merger.dag = readBuffer[4];
            merger.maand = readBuffer[5];
            merger.jaar = readBuffer[6];
            merger.jaar_rest = splitter.jaar_rest;
            DECI(&merger);
            UART_PRINT("\r\n"
                      "Deze tijd en datum is opgeslagen: "
                      "%02d%02x-%02x-%02x %02x:%02x:%02x",merger.jaar_rest, merger.jaar, merger.maand, merger.dag, merger.uur, merger.minuut, merger.sec);
        }
    }
}



void MaakTaak(_u8 prioriteit, _u16 stackSize, void *(*functie)(void *args))
{
    pthread_t Thread;
    pthread_attr_t attrs;
    struct sched_param priParam;
    int retc;

    // sl_task thread maken. Dit handelt alle callbacks van wifi af
    priParam.sched_priority = prioriteit;
    retc = pthread_attr_init(&attrs);
    retc |= pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_DETACHED); // Ontkoppelde taak
    retc |= pthread_attr_setschedparam(&attrs, &priParam); // Prioriteit instellen
    retc |= pthread_attr_setstacksize(&attrs, stackSize); // Stacksize instellen

    retc |= pthread_create(&Thread, &attrs, functie, NULL); // Taak starten
    if (retc != 0)
    {
        // pthread_create() failed
        UART_PRINT("Taak fout: %d",retc);
        while (1);
    }
}

int StelWifiIn(SlWlanSecParams_t* SecParams, SlWlanSecParamsExt_t* SecExtParams, _i8* wachtwoord, bool vraagOmWachtwoord)
{
    int Status;
    SlDateTime_t dateTime = {0};
    _u8 serverAuthenticatie = 0;

    // NWP starten en configureren
    sl_Start(0, 0, 0);
    Status = sl_WlanSetMode(ROLE_STA); // Station modus

    /* Om te voorkomen dat jouw wachtwoord in je code is te vinden wordt het
     * geschreven naar een bestand op de flash.
     *
     * Je moet eenmalig het wachtwoord geven, elke keer daarna kun je het
     * alleen uitlezen. Als je het wilt aanpassen dan moet parameter vraagOmWachtwoord
     * true worden.
     * */
    leesWachtwoord((char *)wachtwoord, vraagOmWachtwoord);

    SecParams->Key = wachtwoord;
    SecParams->KeyLen = strlen((const char*)wachtwoord);
    SecParams->Type = SL_WLAN_SEC_TYPE_WPA_WPA2;

    // extended alleen nodig voor enterprise wifi
    SecExtParams->User = IDENTITY;
    SecExtParams->UserLen = strlen(IDENTITY);
    SecExtParams->AnonUser = 0;
    SecExtParams->AnonUserLen = 0;
    SecExtParams->EapMethod = SL_WLAN_ENT_EAP_METHOD_PEAP0_MSCHAPv2;

    // Jaar instellen zodat het certificaat niet wordt afgekeurd
    dateTime.tm_year =  (_u32)2019; // Year (YYYY format)

    // NWP herstarten om STA modus ook te gebruiken
    sl_Stop(0);
    sl_Start(0, 0, 0);

    // Tijd opslaan
    sl_DeviceSet(SL_DEVICE_GENERAL,
                 SL_DEVICE_GENERAL_DATE_TIME,
                 sizeof(SlDateTime_t),
                 (_u8 *)(&dateTime));

    // Eduroam geeft geen CA certificaat uit, dus check uitzetten
    Status = sl_WlanSet(SL_WLAN_CFG_GENERAL_PARAM_ID, SL_WLAN_GENERAL_PARAM_DISABLE_ENT_SERVER_AUTH, 1, &serverAuthenticatie);

    return Status;
}
